// +build !noprometheus

package instrument

import (
	log "github.com/cihub/seelog"

	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

const (
	promAddr = ":9123"
)

type promInst struct {
	visits    *prometheus.CounterVec
	reqDur    *prometheus.HistogramVec
	search    *prometheus.CounterVec
	searchDur *prometheus.GaugeVec
}

func Init() Instrument {
	go promHandle()

	visits := prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "trantor_visits_total",
			Help: "Number of visits.",
		},
		[]string{"section"},
	)
	reqDur := prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name: "trantor_request_duration_seconds",
			Help: "Duration of the request in seconds.",
		},
		[]string{"section"},
	)
	search := prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "trantor_search_total",
			Help: "Number of searches.",
		},
		[]string{"search"},
	)
	searchDur := prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "trantor_search_duration_seconds",
			Help: "Duration of the search in seconds.",
		},
		[]string{"search"},
	)

	prometheus.MustRegister(visits)
	prometheus.MustRegister(reqDur)
	prometheus.MustRegister(search)
	prometheus.MustRegister(searchDur)

	return &promInst{
		visits:    visits,
		reqDur:    reqDur,
		search:    search,
		searchDur: searchDur,
	}
}

func promHandle() {
	server := http.Server{
		Addr:    promAddr,
		Handler: promhttp.Handler(),
	}
	log.Error(server.ListenAndServe())
}

func (in promInst) Request(req RequestData) {
	in.visits.WithLabelValues(req.Section).Inc()
	in.reqDur.WithLabelValues(req.Section).Observe(req.Duration.Seconds())
	in.search.WithLabelValues(req.Search).Inc()
	in.searchDur.WithLabelValues(req.Search).Set(req.Duration.Seconds())
}
