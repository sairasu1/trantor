package trantor

import (
	log "github.com/cihub/seelog"

	"net/http"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/trantor/trantor/lib/database"
	"gitlab.com/trantor/trantor/lib/parser"
)

func deleteHandler(h handler) {
	ids := strings.Split(mux.Vars(h.r)["ids"], "/")
	submissionID := mux.Vars(h.r)["submissionID"]
	if !h.sess.IsModerator() && !h.booksInSubmission(ids, submissionID) {
		notFound(h)
		return
	}

	var titles []string
	var isNew bool
	for _, id := range ids {
		if id == "" {
			continue
		}
		book, err := h.db.GetBookID(id)
		if err != nil {
			h.sess.Notify("Book not found!", "The book with id '"+id+"' is not there", "danger")
			continue
		}
		err = h.db.UpdateSubmissionByBook(id, "Rejected", nil)
		if err != nil {
			log.Error("There was a problem updating the submission: ", err)
		}
		h.store.Delete(id)
		h.db.DeleteBook(id)

		if !book.Active {
			isNew = true
		}
		titles = append(titles, book.Title)
	}
	if titles != nil {
		h.sess.Notify("Removed books!", "The books "+strings.Join(titles, ", ")+" are completly removed", "success")
	}
	h.sess.Save(h.w, h.r)
	if submissionID != "" {
		http.Redirect(h.w, h.r, "/submission/"+submissionID, http.StatusFound)
	} else if isNew {
		http.Redirect(h.w, h.r, h.r.Referer(), http.StatusFound)
	} else {
		http.Redirect(h.w, h.r, "/", http.StatusFound)
	}
}

type editData struct {
	S            Status
	Book         database.Book
	SubmissionID string
	Tags         []string
}

func editHandler(h handler) {
	id := mux.Vars(h.r)["id"]
	submissionID := mux.Vars(h.r)["submissionID"]
	if !h.sess.IsModerator() && !h.booksInSubmission([]string{id}, submissionID) {
		notFound(h)
		return
	}
	book, err := h.db.GetBookID(id)
	if err != nil {
		notFound(h)
		return
	}

	var data editData
	data.Book = book
	data.S = GetStatus(h)
	data.SubmissionID = submissionID
	data.Tags, err = h.db.GetTags()
	if err != nil {
		log.Error("Error getting tags: ", err)
	}
	author := ""
	if len(book.Authors) > 0 {
		author = " by " + book.Authors[0]
	}
	data.S.Title = book.Title + author + " -- Edit -- " + data.S.Title
	h.load("edit", data)
}

func cleanEmptyStr(s []string) []string {
	var res []string
	for _, v := range s {
		if v != "" {
			res = append(res, strings.TrimSpace(v))
		}
	}
	return res
}

func saveHandler(h handler) {
	// XXX: check for errors (ISBN, length(lang), ...)
	id := mux.Vars(h.r)["id"]
	submissionID := mux.Vars(h.r)["submissionID"]
	if !h.sess.IsModerator() && !h.booksInSubmission([]string{id}, submissionID) {
		notFound(h)
		return
	}

	title := h.r.FormValue("title")
	publisher := h.r.FormValue("publisher")
	date := h.r.FormValue("date")
	description := h.r.FormValue("description")
	authors := cleanEmptyStr(h.r.Form["author"])
	tags := cleanEmptyStr(strings.Split(h.r.FormValue("tags"), ","))
	isbn := parser.ISBN(h.r.FormValue("isbn"))
	lang := h.r.FormValue("lang")
	book := map[string]interface{}{"title": title,
		"publisher":   publisher,
		"date":        date,
		"description": description,
		"authors":     authors,
		"tags":        tags,
		"isbn":        isbn,
		"lang":        lang}
	err := h.db.UpdateBook(id, book)
	if err != nil {
		log.Error("Updating book: ", err)
		h.sess.Notify("Can't modify book!", err.Error(), "danger")
	} else {
		h.sess.Notify("Book Modified!", "", "success")
	}

	h.sess.Save(h.w, h.r)
	if submissionID != "" {
		http.Redirect(h.w, h.r, "/submission/"+submissionID, http.StatusFound)
	} else if h.db.IsBookActive(id) {
		http.Redirect(h.w, h.r, "/book/"+id, http.StatusFound)
	} else {
		// XXX: I can't use a referer here :(
		http.Redirect(h.w, h.r, "/submission/moderate/", http.StatusFound)
	}
}
